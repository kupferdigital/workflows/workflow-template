inputFilesChannel = Channel.fromList(params.inputFiles).collect()

process mapToRdf {
 
    container params.registry + '/rml:latest'

    publishDir 'output', mode: 'copy'

    input:
    path files from inputFilesChannel

    output:
    path 'output.ttl' into output

    """
    rml -d -v --mapping mapping.ttl --output output.ttl
    """

}
