## Workflow Template

This is an minimal example for a RDF transformation workflow.


```bash
# Install nextflow
curl -s https://get.nextflow.io | bash
# Get config file
curl https://gitlab.com/kupferdigital/data-ecosystem/workflows/workflow-template/-/raw/main/example/config.json > config.json
# Run the workflow
nextflow run -params-file config.json https://gitlab.com/kupferdigital/data-ecosystem/workflows/workflow-template -r main
```
